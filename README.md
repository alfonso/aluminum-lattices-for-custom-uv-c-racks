# **Aluminum Lattices for Custom UV-C Racks**
## Introduction and problem explanation.
Kohshi, a CBA friend from Toyota, shared with us a need that the Toyota hospital is facing. Find attached the initial request :

> Today I discussed with the vice director of Toyota Hospital to know their concern and difficulty of dealing with COVID-19.
**They are trying to sterilize following devices** several times in a day by alcohol but not sure if it is perfectly done all the times;

>  * Note-PC/i-Pad (to share the status with patients)
>  * sphygmomanometer (especially the band with fabric part is difficult to  sterilize with alcohol)
>  * mask
>  * medical thermometer
>  * curtain (to separate neighboring beds)
etc.

>  Then, I thought attached drawing **"Lattice Frame/Shelf UV-C Sterilizer Rack** must be useful.
Lattice (should be aluminum not to be damaged by UV light) shelf with UV-C light inside might be efficient to sterilize most of the surfaces of medical devices at the same time so that the rack must be efficient for space and stiffness.
With four or three pillar lattice could sterilize curtain one size then another quite efficiently.
Based on below information (sorry this is in Japanese), UV-C light with 260nm wave length at the distance of 15cm for 2 minutes deactivate influenza virus into 1/10000.


Kohshi also shared a really interesting idea that is use the internal volume that naturally the voxels have in order to allocate there the UV lights. Here you can find his sketches.

![Sketches by Kohshi](img/sk1.png)
![Sketches by Kohshi](img/sk2.png)
![Sketches by Kohshi](img/sk3.png)

Given this  proposals, we need to **split the problem**. On the one hand we need to find a **solution for making metallic voxels**. Due to the easiest metal to recycle now is aluminum, I am going to dive in this material for a while. Also we need to find information about the **protection needed for UV-C light with 260nm wavelength**. Besides, a better understanding on if we need a reluctant material or an absorbing material will help us to define a faster solution for this.


## **Structure.** Metallic discrete building blocks.

***04/28***

After discarding the waterjet process due to many factors, its been decided to use the Fablight laser cutter to work with. This allow us to make tiny features so we can iterate much more and better on the hinges.

<img src="img/st1.png" alt="LAttice" height="400" >

This process has been done folding the voxel using the comented living hinges and ribeting it in the zones that requires an union but a hinge is not present. Still being a need on study the hinge geometry and how it affects to the voxel behaviour.

<img src="img/st2.png" alt="LAttice" height="400" >

The same cube develop was laser cut and tooling was designed and manufactured to bend 45º the rivet connections between faces.

<img src="img/45bend.jpeg" alt="LAttice" height="500" >

Using the manual press the gray part comes down and provided the desired shape. The white piece holds the geometry precisely and doesn't allow to bend in any other space direction.

<img src="img/toolingpre.jpeg" alt="LAttice" height="500" >
<img src="img/piecepost.jpeg" alt="LAttice" height="500" >







***04/26***

It can be really interesting proposing one building block and its attachment so people could be able to build custom racks under their needs and circumstances. Proposing responsively this idea requires to characterize them to guarantee a safe behavior under some allowable values.

First of all, we are diving in the shape proposed. Ive been concerned about proposing a lattice trying to ensure the maximum use of raw material as possible. Also, deciding to go to metal will force us to use a powerful laser cutter or a waterjet. Also, there is a chance to try to reduce the assembly process by folding the voxel in specific areas.


Re-visiting the Archimedean solids to inspire us, again finally will be chosen to start with Ben's election, the cuboctahedron.

<img src="img/map2.jpeg" alt="LAttice" height="560" >

This proposal is based on a folded / riveted voxel.

<img src="img/map.jpeg" alt="LAttice" height="560" >

This flat map gives the chance to develop a simple machine able to fold the cube till the final cube form by doing two simple folds and offset the face one as its described in the following scheme:

<img src="img/mount.gif" alt="LAttice"  >


MORE INFO TO BE UPLOADED TOMORROW APRIL 26
